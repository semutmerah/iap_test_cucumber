module IapTokenGenerator
  extend self
  @@openid_token_url = "https://www.googleapis.com/oauth2/v4/token".freeze

  def get_private_key(file)
    data = File.read(file)
    parse = JSON.parse(data)
    private_key = OpenSSL::PKey::RSA.new parse["private_key"]
    return private_key
  end
  
  def get_client_email(file)
    data = File.read(file)
    parse = JSON.parse(data)
    return parse["client_email"]
  end

  def now
    return Time.now.to_i
  end

  def generate_jwt
    payload = { iss: get_client_email(ENV['SERVICE_ACCOUNT_FILE']), aud: @@openid_token_url, iat: now, exp: now + 300, target_audience: ENV['CLIENT_ID'] }
    jwt_token = JWT.encode payload, get_private_key(ENV['SERVICE_ACCOUNT_FILE']), 'RS256'
    return jwt_token
  end

  def generate_token_openid
    response = HTTParty.post(@@openid_token_url, body: {
      grant_type: "urn:ietf:params:oauth:grant-type:jwt-bearer",
      assertion: generate_jwt
    })
    body = JSON.parse(response.body)
    return body["id_token"]
  end
end
