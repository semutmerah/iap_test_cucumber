require 'browsermob/proxy'
require 'capybara'
require 'capybara/cucumber'
require 'capybara/dsl'
require 'capybara/rspec/matchers'
require 'dotenv'
require 'httparty'
require 'json'
require 'jwt'
require 'rake'
require 'rspec'
require 'selenium-webdriver'
require 'site_prism'
require 'webdrivers'
require_relative '../helper/iaptokengenerator'

Dotenv.load

World(Capybara::DSL)
World(Capybara::RSpecMatchers)

browser = ENV['BROWSER'].to_sym

server = BrowserMob::Proxy::Server.new(File.dirname(__FILE__) + "/../../bin/browserup-proxy/bin/browserup-proxy", :log => true)

begin
  server.start
rescue Exception => e
  Selenium::WebDriver::Wait.new(:timeout => 20).until do
    begin
      HTTParty.get("http://localhost:8080/proxy").success?
    rescue
    end
  end
end

token = IapTokenGenerator.generate_token_openid
token_bearer = "Bearer " + token
proxy = server.create_proxy
proxy.headers({"Authorization" => token_bearer})
prx = proxy.selenium_proxy :http, :ssl
Capybara.register_driver :chrome do |app|
  browser_options = ::Selenium::WebDriver::Chrome::Options.new.tap do |opts|
    opts.args << '--window-size=1366,768'
    opts.args << '--disable-gpu'
    opts.args << '--no-sandbox'
    opts.args << '--disable-dev-shm-usage'
    opts.args << '--disable-notifications'
    opts.args << '--disable-extensions'
  end
  caps = Selenium::WebDriver::Remote::Capabilities.chrome(
    proxy: prx,
    accept_insecure_certs: true
  )
  Capybara::Selenium::Driver.new(
    app,
    browser: :chrome,
    options: browser_options,
    desired_capabilities: caps
  )
end

Capybara.register_driver :firefox do |app|
  options = Selenium::WebDriver::Firefox::Options.new
  options.add_preference 'dom.webnotifications.enabled', false
  options.add_preference 'dom.push.enabled', false
  caps = Selenium::WebDriver::Remote::Capabilities.firefox(
    proxy: prx,
    accept_insecure_certs: true
  )
  Capybara::Selenium::Driver.new(
    app,
    browser: :firefox,
    desired_capabilities: caps,
    options: options
  )
end

at_exit do
  proxy.close
  server.stop
end

Capybara.configure do |config|
  config.default_driver =  browser
  config.default_max_wait_time = 10
end
