FROM ruby:2.5.7-alpine
LABEL version="1.0"

RUN apk update \
&& apk upgrade

RUN apk add --no-cache \
libstdc++==9.2.0-r4 \
chromium==79.0.3945.130-r0 \
chromium-chromedriver==79.0.3945.130-r0 \
harfbuzz==2.6.4-r0 \
nss==3.48-r0 \
freetype==2.10.1-r0 \
ttf-freefont==20120503-r1 \
openjdk11==11.0.5_p10-r0 \
build-base

ENV JAVA_HOME=/usr/lib/jvm/java-11-openjdk \
    CHROME_BIN=/usr/bin/chromium-browser \
    CHROME_PATH=/usr/lib/chromium/

COPY Gemfile* /

RUN bundle install

# ENTRYPOINT ["chromium-browser", "--headless", "--disable-gpu", "--disable-software-rasterizer", "--disable-dev-shm-usage", "--no-sandbox"]
